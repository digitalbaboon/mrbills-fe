(ns mrbills.routes
  (:require 
    [secretary.core :as secretary]
    [mrbills.route :as route])
  (:require-macros 
    [secretary.core :refer [defroute]]))

(defn get [state]
  "Define routes"
  (defroute "/" []
    (swap! state assoc :page :about-page))
    
  (route/nav! (str js/window.location.pathname) state))