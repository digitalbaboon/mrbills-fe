(ns mrbills.route
  (:require [secretary.core :as secretary :refer-macros [defroute]]
            [goog.events])
  (:import [goog.history Html5History EventType]))

(defn get-token []
  (str js/window.location.pathname js/window.location.search))

(defn make-history []
  (doto (Html5History.)
    (.setPathPrefix (str js/window.location.protocol "//" js/window.location.host))
    (.setUseFragment false)))

(defn handle-url-change [e]
  (when-not (.-isNavigation e)
    (js/window.scrollTo 0 0))
  (secretary/dispatch! (get-token)))

(defonce history
  (doto
    (make-history)
    (goog.events/listen EventType.NAVIGATE
                        #(handle-url-change %))
    (.setEnabled true)))

(defn nav! [token state]
  (swap! state assoc :current-path token)
  (.setToken history token))