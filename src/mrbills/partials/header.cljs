(ns mrbills.partials.header)

(defn init! [state]
  [:div.container
    [:div.header
      [:h2.logo
        [:a {:href "/"} "Mr. Bills"]]
      [:div.menu
        [:ul
          [:li
            [:a {:href "/"} "About"]]
          [:li
            [:a {:href "/pricing"} "Pricing"]]
          [:li
            [:a {:href "/blog"} "Blog"]]
          [:li.sep]
          [:li.light
            [:a {:href "/signin"} "Sign In"]]
          [:li.light
            [:a {:href "/signup"} "Sign Up"]]]]]])