(ns mrbills.pages.about
  (:require
    [mrbills.partials.header :as header]))

(defn- page! [state]
  "Paint the page content"
  [:div
    [:h2 ":)"]])

(defn init! [state]
  "Paint the about page"
  [:div
    (header/init! state)
    (page! state)])