(ns mrbills.core
    (:require
      [reagent.core :as r]
      [alandipert.storage-atom :refer [local-storage]]
      [mrbills.routes :as routes]
      [mrbills.pages.about :as about-page]))

(def state (local-storage (r/atom {}) :state))

(defmulti current-page #(@state :page))
(defmethod current-page :about-page [state]
  [about-page/init! state])

(defn- pre-init! []
  "Things to do before we go woo!"
  (when (nil? (:token @state))
    (swap! state assoc :token "0"))

  (when (empty? (:notification @state))
    (swap! state assoc :notification nil))
    
  (routes/get state))
    
(defn init! []
  "Initialize the whole shabang"
  (pre-init!)

  (r/render [current-page state]
    (.getElementById js/document "app")))