(ns ^:figwheel-no-load mrbills.dev
  (:require
    [mrbills.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
