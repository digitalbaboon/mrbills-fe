# Mr. Bills Front-End

This is the whole front-end of [Mr. Bills - the helpful financial tool](https://www.mrbills.xyz) that 
I'm building. It is here for a few reasons;

- If you wanted to, you could contribute to the project's development
- Considering how little ClojureScript projects there are out there, this may serve as a place for somebody to learn as I try to keep my code as simple as possible
- The code here is also free, so if you wanted to take this and go make something from this yourself, you are free to do so

## API

I will document the entire API here as soon as the API is more-or-less finished.